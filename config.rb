require 'bourbon'
require 'neat'
require 'bitter'

# define bourbon path, use a common library for preventing duplicates

### -->> project paths, delete/comment if you use scout, codekit etc.
environment = :production # removes single line comments, keeps css comments
preferred_syntax = :scss
project_type = :stand_alone
output_style = :compact
relative_assets = true

http_path = "/"
css_dir = "css" #relative to project root
scss_dir = "scss" #relative to project root
images_dir = "images" #relative to project root
javascripts_dir = "js" #relative to project root
### <<-- project paths, ignore/comment if you use scout.app or codekit etc.

# where the fonts resides
fonts_dir = "fonts"

line_comments = false

# for debug options in chrome web developer tools
sass_options = {
  :debug_info => false,
	:sourcemap => flase #with SASS > 3.3 http://benfrain.com/add-sass-compass-debug-info-for-chrome-web-developer-tools/
}
