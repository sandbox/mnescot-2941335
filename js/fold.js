(function($) {
  var fold = $('.fold-menu > div').prev();
  fold.addClass('dir').attr('href', '#');
  fold.on('click', function(event) {
    event.preventDefault();
    var $this = $(this);
    $this.toggleClass('hide');
    $this.next().toggle('300');
  });
})(jQuery);
