//
jQuery(".flexnav")
	.flexNav();
jQuery(".menu-button")
	.click(function() {
		jQuery(this)
			.toggleClass("menu-on");
		jQuery("i")
			.remove(".navicon");
	});
jQuery(".item-with-ul")
	.click(function(e) {
		jQuery(this)
			.find('span')
			.toggleClass("opened");
	});
//
jQuery(document)
	.ready(function() {
        jQuery('body')
            .addClass('js');
        var $menu = jQuery('.sidenav');
        $menulink = jQuery('.menu-link');
        $menulink.click(function () {
            $menulink.toggleClass('active');
            $menu.toggleClass('active');
            return false;
        });
    });
//
(function($) {
	'use strict';
	jQuery(window)
		.on('load', (function() {
			jQuery('.masonry')
				.masonry({
					columnWidth: '.grid-sizer',
					gutter: '.gutter-sizer',
					itemSelector: '.item'
				});
		}));
}(jQuery));
//
jQuery('.anchor-link').click(function(e){
  e.preventDefault();
  var target = jQuery(jQuery(this).attr('href'));
  if(target.length){
    var scrollTo = target.offset().top;
    jQuery('body, html').animate({scrollTop: scrollTo+'px'}, 800);
  }
});
//
